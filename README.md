# Bookworm

Bookworm is an app to track which books the user has read and what he thought of them.

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/bookworm-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- @Binding
- TextEditor
- SwiftData, 
- List
- \@Binding
- Sorting
- Deleting